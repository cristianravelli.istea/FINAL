# Indicamos la imagen a utilizar de base
FROM ubuntu:18.04
# Info acerca de la imagen
LABEL maintainer="Cristian"
LABEL version="1.0"
ARG DEBIAN_FRONTEND=noninteractive
# instalación de paquetes
RUN apt update && apt install -y apache2 php libapache2-mod-php && apt clean && apt autoremove
# Puntos de montaje
#VOLUME ["/var/www/html"]
VOLUME ["/webapp"]
# Directorio 'app'
RUN mkdir -p /webapp
# Copiar archivos al directorio 'app'
COPY webapp /webapp
# Copiar default vhost al contenedor
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf
# Copiar configuracion de apache 
COPY apache2.conf /etc/apache2/apache2.conf
# Exponer puertos
EXPOSE 80
# Comando a ejecutarse cuando se crea el contenedor
CMD ["apachectl", "-D", "FOREGROUND"]
