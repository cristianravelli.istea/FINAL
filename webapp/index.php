<?php

// Comment these lines to hide errors
error_reporting(E_ALL);
ini_set('display_errors', 1);

require '/webapp/includes/config.php';
require '/webapp/includes/functions.php';

init();
