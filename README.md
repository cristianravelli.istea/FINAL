TP Final

Ubuntu
Apache
PHP

1. Descargar/Clonar el proyecto
2. Ingresar a la carpeta 
3. Seguir los siguientes comandos

Construir la imagen:
```
docker build -t webapp/cristian .
```

Levantar volumen
```
docker volume create cristian_webapp_volume
```

Generar container
```
docker run --name cristian_webapp -dit -v cristian_webapp_volume:/webapp -p 83:80 webapp/cristian
```

Web Site Example: https://github.com/banago/simple-php-website
